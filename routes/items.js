var express = require('express')
    , router = express.Router();
var apiModule = require('../utils/apiModule');

/* GET users listing. */
router.get('/:tableNickname', function(req, res, next) {
    var tableNickname = req.params.tableNickname;
    apiModule.getByName('/items').getItemsInfo(tableNickname, req.query.page,
        function(err, itemMetadataList, pages) {
            if (err) {
                next(err);
                return;
            }
            
            res.render('items', {
                title: 'Express',
                tNick: tableNickname,
                itemMetadataList: itemMetadataList,
                totalPageCount: pages.getTotalPageCount(),
                currentPage: pages.getCurrentPage()
            });
        });
});

router.get('/:tableNickname/:itemId', function(req, res, next) {
    var tableNickname = req.params.tableNickname;
    apiModule.getByName('/items').getItemInfo(tableNickname, req.params.itemId,
        function(err, item) {
            if (err) {
                next(err);
                return;
            }
            
            res.render('item', { tNick: tableNickname, item: item });
        });
});

module.exports.path = '/items';
module.exports.router = router;
