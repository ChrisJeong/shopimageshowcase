var express = require('express');
var router = express.Router();

/* GET file. */
router.get('/:tNick/subitem/:fId', function(req, res, next) {
    var db = require('../models/db');
    db.connectToDatabase()
    .then(function(dbWrapper) {
        var tableNickname = req.params.tNick;
        var fsId = req.params.fId;
        
        db.getFileData(dbWrapper, tableNickname, fsId, function(err, data) {
            if (err) {
                return;
            }
            res.writeHead('200');
            res.end(data, 'binary');
        });
    });
});

module.exports.path = '/images';
module.exports.router = router;
