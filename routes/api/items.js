var express = require('express');
var router = express.Router()
    , Q = require('q')
    , Pages = require('../../models/pages')
    , db = require('../../models/db')
    , apiError = require('../../models/apiError').createApiError;

var itemCountPerPage = 4 * 4;

var itemSkipCountForPage = function(page) {
    return (page - 1) * itemCountPerPage;
};

router.get('/:tableNickname', function(req, res, next) {
    const tableNickname = req.params.tableNickname;
    getItemsInfo(tableNickname, req.query.page, function(err, itemMetadataList, pages) {
        if (err) {
            next(err);
        } else {
            var refactoredItems = itemMetadataList
                .map(function(itemMetadata) {
                    if (!itemMetadata.primarySubitem) {
                        delete itemMetadata.primarySubitem;
                    } else {
                        itemMetadata.primarySubitem =
                            createSubitem(tableNickname, itemMetadata.primarySubitem)
                    }
                    return itemMetadata;
                });
            res.json({
                itemMetadataList: refactoredItems,
                pages: pages
            });
        }
    });
});

router.get('/:tableNickname/:itemId', function(req, res, next) {
    var tableNickname = req.params.tableNickname;
    getItemInfo(tableNickname, req.params.itemId, function(err, item) {
        if (err) {
            next(err);
        } else {
            var subitems = item.subitems.map(function(subitem) {
                return createSubitem(tableNickname, subitem);
            });
            res.json({
                itemId: item.itemId,
                subitems: subitems
            });
        }
    });
});

function getItemsInfo(tableNickname, pageIdx, callback) {
    db.connectToDatabase()
    .then(function(dbWrapper) {
        dbWrapper.loadCollectionCount(tableNickname)
        .then(function(totalItemCount) {
            if (totalItemCount == 0) {
                throw apiError({ message: "No items available." });
            }
            
            var pages = new Pages(totalItemCount, itemCountPerPage, pageIdx || 1);
            var skipCount = itemSkipCountForPage(pages.getCurrentPage());
            return Q.all([
                Q(pages),
                // TODO: this retrieves all subitems for each item. Consider refactor.
                dbWrapper.getItems(tableNickname, skipCount, itemCountPerPage)
            ]);
        })
        .then(function(values) {
            var pages = values[0];
            var items = values[1];
            var itemMetadataList = items.map(function(item) {
                return {
                    itemId: item.itemId,
                    primarySubitem: dbWrapper.getPrimarySubitem(item)
                };
            });
            
            callback(null, itemMetadataList, pages);
        }, function(err) {
            callback(err);
        });
    });
}

function getItemInfo(tableNickname, itemId, callback) {
    db.connectToDatabase()
    .then(function(dbWrapper) {
        dbWrapper.getItem(tableNickname, itemId, function(err, item) {
            if (err || !item) {
                callback(apiError({ message: "No item available." }));
            } else {
                callback(null, item);
            }
        });
    });
}

function createSubitem(tableNickname, subitem) {
    return {
        imageName: subitem.subitemName,
        imagePath: getImagePath(tableNickname, subitem.fsId)
    };
}

function getImagePath(tableNickname, itemId) {
    return '/images/' + tableNickname + '/subitem/' + itemId;
}

module.exports.getItemsInfo = getItemsInfo;
module.exports.getItemInfo = getItemInfo;

module.exports.path = '/api/items';
module.exports.router = router;
