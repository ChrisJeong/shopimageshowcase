var express = require('express');
var router = express.Router();

router.get('/', function(req, res, next) {
    var databaseInfos = getMallInfo();
    var filteredDatabaseInfos = databaseInfos.map(function(info) {
        return {
            nickname: info.nickname,
            name: info.name
        };
    });
    
    res.json(filteredDatabaseInfos);
});

function getMallInfo() {
    return require('../../settings').database.tables;
}

module.exports.getMallInfo = getMallInfo;

module.exports.path = '/api/malls';
module.exports.router = router;
