var express = require('express');
var router = express.Router();
var apiModule = require('../utils/apiModule');

router.get('/', function(req, res, next) {
    var databaseInfo = apiModule.getByName('/malls').getMallInfo();
    res.render('malls', { title: 'Express', databaseInfos: databaseInfo });
});

module.exports.path = '/malls';
module.exports.router = router;
