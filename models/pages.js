var Pages = function(totalItemCount, itemCountPerPage, currentPageRaw) {
    this.totalItemCount = totalItemCount;
    this.itemCountPerPage = itemCountPerPage;
    this.currentPageRaw = currentPageRaw;
};

Pages.prototype.getTotalPageCount = function() {
    return Math.floor(this.totalItemCount / this.itemCountPerPage) + (this.totalItemCount % this.itemCountPerPage == 0 ? 0 : 1);
};

Pages.prototype.getCurrentPage = function() {
    return Math.min(Math.max(this.currentPageRaw, 1), this.getTotalPageCount());
};

module.exports = Pages;
