var mongodb = require('../node_modules/mongodb')
    , MongoClient = mongodb.MongoClient
    , ObjectID = mongodb.ObjectID
    , GridStore = mongodb.GridStore;

var file = this;

var MongoDb = function MongoDatabase(settings) {
    this.settings = settings;
    var connectionInfo = settings.connectionInfo;
    this.host = connectionInfo[0];
    this.port = connectionInfo[1];
};

MongoDb.prototype.connect = function(resolve, reject) {
    var mongoDb = this;
    var host = process.env.MONGO_HOST || this.host;
    var url = host + ":" + this.port;
    MongoClient.connect(url, function(err, adminDb) {
        if (err != null) {
            reject(err);
        } else {
            mongoDb.adminDb = adminDb;
            resolve(mongoDb);
        }
    });
};

MongoDb.prototype.getTableSettings = function(tableNickname) {
    return this.settings.getTable(tableNickname);
};

MongoDb.prototype.getSchema = function(tableNickname) {
    return this.getTableSettings(tableNickname).schema;
};

MongoDb.prototype.getTable = function(tableNickname) {
    var tableSettings = this.getTableSettings(tableNickname);
    return this.adminDb.db(tableSettings.name);
};

MongoDb.prototype.getCollection = function(tableNickname) {
    var schema = this.getSchema(tableNickname);
    var collectionName = schema.collectionName;
    var table = this.getTable(tableNickname);
    return table.collection(collectionName);
};

MongoDb.prototype.loadCollectionCount = function(tableNickname) {
    var dbWrapper = this;
    return new Promise(function(resolve) {
        dbWrapper.getCollection(tableNickname).find().count(function(err, count) {
            if (err) {
                resolve(0);
            } else {
                resolve(count);
            }
        });
    });
};

MongoDb.prototype.iterateItems = function(tableNickname, from, count, callback) {
    var mongoDb = this;
    
    var schema = this.getSchema(tableNickname);
    var sortOption = {};
    sortOption[schema.collectionInfo.id] = 1;
    
    this.getCollection(tableNickname).find().sort(sortOption).skip(from).limit(count)
        .each(function(err, rawItem) {
            if (err) {
                callback(err, null);
                return;
            }
            if (!rawItem) {
                callback(null, null);
                return;
            }
            callback(null, new MongoItem(mongoDb.settings, tableNickname, rawItem));
    });
};

MongoDb.prototype.getItems = function(tableNickname, from, count) {
    var mongoDb = this;
    return new Promise(function(resolve, reject) {
        var items = [];
        
        mongoDb.iterateItems(tableNickname, from, count, function(err, mongoItem) {
            if (err) {
                reject(err);
                return;
            }
            if (!mongoItem) {
                resolve(items);
                return;
            }
            items.push(mongoItem);
        });
    });
};

MongoDb.prototype.getItem = function(tableNickname, itemId, callback) {
    var mongoDb = this;
    
    var schema = this.getSchema(tableNickname);
    var query = {};
    query[schema.collectionInfo.itemId] = itemId;
    
    this.getCollection(tableNickname).findOne(query, function(err, rawItem) {
        if (err) {
            callback(err, null);
            return;
        }
        if (!rawItem) {
            callback(null, null);
            return;
        }
        
        callback(null, new MongoItem(mongoDb.settings, tableNickname, rawItem));
    });
};

MongoDb.prototype.getPrimarySubitem = function(mongoItem, callback) {
    return mongoItem.getPrimarySubitem(this);
};

var MongoItem = function MongoItem(dbSettings, tableNickname, rawItem) {
    var collectionInfo = dbSettings.getTable(tableNickname).schema.collectionInfo;
    var subitemsInfo = collectionInfo.subitemsInfo;
    
    var rawSubitems = rawItem[collectionInfo.subitemsName];
    
    var subitems = [];
    for (var i = 0; i < rawSubitems.length; i++) {
        var rawSubitem = rawSubitems[i];
        var fsId = rawSubitem[subitemsInfo.fsId];
        var subitemName = rawSubitem[subitemsInfo.subitemName];
        
        subitems.push({
            fsId: fsId,
            subitemName: subitemName
        });
    }
    
    this.tableNickname = tableNickname;
    this.id = rawItem[collectionInfo.id];
    this.itemId = rawItem[collectionInfo.itemId];
    this.subitems = subitems;
};

MongoItem.prototype.getPrimarySubitem = function(mongoDb) {
    var mongoItem = this;
    return this.subitems.filter(function(element) {
        var prefix = mongoDb.getSchema(mongoItem.tableNickname).collectionInfo.primarySubitemPrefix;
        return element.subitemName.startsWith(prefix);
    })[0];
};

var getFileData = function(dbWrapper, tableNickname, fsId, callback) {
    var table = dbWrapper.getTable(tableNickname);
    
    var gridStore = new GridStore(table, new ObjectID(fsId), "r");
    gridStore.open(function(err, result) {
        if (err) {
            callback(err, null);
            return;
        }
        gridStore.seek(0, function() {
            gridStore.read(function(err, data) {
                if (err) {
                    callback(err, null);
                    return;
                }
                callback(null, data);
            });
        });
    });
};

file.MongoDb = MongoDb;

function createDatabaseWrapper(settings) {
    var Database = file[settings.className];
    var database = new Database(settings);
    // var database = new Database(connectionInfo);
    return database;
}

module.exports.MongoItem = MongoItem;

module.exports.connectToDatabase = function(settings) {
    return new Promise(function(resolve, reject) {
        if (typeof file.databaseWrapper != 'undefined') {
            resolve(file.databaseWrapper);
        } else {
            var dbWrapper = createDatabaseWrapper(settings);
            dbWrapper.connect(function(dbWrapper) {
                file.databaseWrapper = dbWrapper;
                resolve(dbWrapper);
            }, reject);
        }
    });
};

module.exports.getFileData = getFileData;
