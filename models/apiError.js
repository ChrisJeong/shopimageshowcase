var util = require( "util" );

exports.ApiError = ApiError;
exports.createApiError = createApiError;

function createApiError( settings ) {
    return( new ApiError( settings, createApiError ) );
}

function ApiError( settings, implementationContext ) {
    settings = ( settings || {} );

    this.name = "ApiError";
    
    this.type = ( settings.type || "Application" );
    this.message = ( settings.message || "An error occurred." );
    this.detail = ( settings.detail || "" );
    this.extendedInfo = ( settings.extendedInfo || "" );
    this.errorCode = ( settings.errorCode || "" );
    
    this.isApiError = true;

    Error.captureStackTrace( this, ( implementationContext || ApiError ) );
}

util.inherits( ApiError, Error );
