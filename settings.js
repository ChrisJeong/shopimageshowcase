var settingsJson = require('./settings.json');

var Settings = function Settings(settingsJson) {
    var db = settingsJson['DATABASE'];
    
    var dbClsName = db['DB_CLASS'];
    var dbConn = db['DB_CONNECTION_SETTINGS'];
    var rawDbInfos = db['TABLE_LIST'];
    
    var connectionInfo = [dbConn['HOST'], dbConn['PORT']];
    var dbInfos = [];
    
    for (var i = 0; i < rawDbInfos.length; i++) {
        var dbInfo = rawDbInfos[i];
        var schema = dbInfo['SCHEMA'];
        var collectionInfo = schema['collection_info'];
        var subitemsInfo = collectionInfo['sub_items'];
        
        dbInfos.push({
                nickname: dbInfo['NICKNAME'],
                name: dbInfo['TABLE_NAME'],
                schema: {
                    collectionName: schema['collection_name'],
                    collectionInfo: {
                        id: collectionInfo['id'],
                        itemId: collectionInfo['item_id'],
                        subitemsName: collectionInfo['subitems_name'],
                        primarySubitemPrefix: collectionInfo['primary_subitems_prefix'],
                        subitemType: collectionInfo['subitems_type'],
                        subitemsInfo: {
                            fsId: subitemsInfo['fs_id'],
                            subitemName: subitemsInfo['sub_item_name']
                        }
                    }
                }
            });
    }
    
    this.database = {
        className: dbClsName,
        connectionInfo: connectionInfo,
        tables: dbInfos,
        getTable: function(nickname) {
            return this.tables.filter(function(element) {
                return element.nickname == nickname;
            })[0];
        }
    };
};

module.exports = new Settings(settingsJson);
